package com.project.Online_Assessment.OnlineAssessment;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
//import org.springframework.data.jpa.repository.JpaRepository;




@Repository 

public interface CandidateRepo extends CrudRepository <Candidate, String>{

	Candidate findByEmail(String email);

	
	

	@Query("from Candidate order by firstname asc")
	List<Candidate> findByName();
}


