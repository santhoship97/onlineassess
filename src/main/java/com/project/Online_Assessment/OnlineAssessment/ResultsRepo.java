package com.project.Online_Assessment.OnlineAssessment;


import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;


public interface ResultsRepo extends CrudRepository<Results , String>{
	
	
	@Query("from Results order by TestDate")
List<Results> findByTestDate();
	
	
	

}
