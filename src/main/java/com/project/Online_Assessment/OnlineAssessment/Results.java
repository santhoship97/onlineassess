package com.project.Online_Assessment.OnlineAssessment;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Results {
	
	@Id
	private String TestDate;
	private String Assessment;
	private int TestMarks;
	private int TotalMarks;
	
	private String email;
	private String result;
	
	
	
	
	public Results(String testDate, String assessment, int testMarks, int totalMarks, String email, String result) {
		super();
		TestDate = testDate;
		Assessment = assessment;
		TestMarks = testMarks;
		TotalMarks = totalMarks;
		this.email = email;
		this.result = result;
	}
	public Results() {
		super();
		
	}
	public String getTestDate() {
		return TestDate;
	}
	public void setTestDate(String testDate) {
		TestDate = testDate;
	}
	public String getAssessment() {
		return Assessment;
	}
	public void setAssessment(String assessment) {
		Assessment = assessment;
	}
	public int getTestMarks() {
		return TestMarks;
	}
	public void setTestMarks(int testMarks) {
		TestMarks = testMarks;
	}
	public int getTotalMarks() {
		return TotalMarks;
	}
	public void setTotalMarks(int totalMarks) {
		TotalMarks = totalMarks;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	
	
	
	

}
