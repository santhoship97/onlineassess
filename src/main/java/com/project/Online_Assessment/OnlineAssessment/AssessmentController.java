package com.project.Online_Assessment.OnlineAssessment;
    import java.time.LocalDateTime;
	import java.time.format.DateTimeFormatter;
	import java.util.List;
	import org.springframework.beans.factory.annotation.Autowired;
	import org.springframework.stereotype.Controller;
	import org.springframework.ui.Model;
	import org.springframework.web.bind.annotation.GetMapping;
	import org.springframework.web.bind.annotation.PostMapping;
	import org.springframework.web.bind.annotation.RequestBody;
	import org.springframework.web.bind.annotation.RequestMapping;
	import org.springframework.web.bind.annotation.RequestMethod;
    import org.springframework.web.bind.annotation.RequestParam;
     import org.springframework.web.bind.annotation.ResponseBody;







	@Controller
	public class AssessmentController {
	    
		@Autowired
		 CandidateRepo canrepo;
		
		@Autowired
		 ResultsRepo resrepo;
		

		
	@RequestMapping("/")
		public String index()
		{
		  return "index";
		}
		
		@RequestMapping("/OnlineAssessment")
		public String index1()
		{
		  return "index";
		}
		
		@RequestMapping("/index.html")
		public String index2()
		{
		  return "index";
		}
		
	String Email="";
		

		
		int i=0;
		@PostMapping("/index.html")
		public String Login(@RequestParam("email") String email,@RequestParam("password") String password)
		{
			Candidate c= canrepo.findByEmail(email);
			Email=email;
			if(c==null)
			{
				i=2;
				return "redirect:/login/do";
			}
			if(c.getPassword().equals(password)!=true)
			{
				i=1;
				return "redirect:/login/do";
				
			}
			if(c.getUserType().equals("admin"))
			{
				i=3;
				return "redirect:/login/do";
				
			}
			return "SelectTest";
		}
			
				@GetMapping("/login/do")	
				
				public String Seperate()
				{
					if(i==2)
					{
						return "InvalidUser";
						
					}
					else if(i==1)
					{
						return "PasswordWrong";
					}
					
					else
					{
						return "SelectReport";
				    }
				 
		         }
				
	       @RequestMapping("/register.html")
	        
	       public String Register()
	       {
	    	   return "register";
	       }
	       
	       String fname="";
	    	String lname="";
		
	    	
	    	@PostMapping("/register.html")
	    	public String register(String firstname,String lastname,String email,String password,Model m)
	    	{
	    		Candidate c = new Candidate(firstname,lastname,email,password,"candidate");

	    		
	    		canrepo.save(c);
	  		fname = firstname;
	  		lname = lastname;
//	  		canrepo.save(c);
	    		return "redirect:/registration/do";
	    	}
	    	
		
	    	@GetMapping("/registration/do")
	    	public String registerSuccess(Model m)
	    	{
//	    		String name=fname + " "+ lname;
//	    		m.addAttribute("name");
  		m.addAttribute("name",fname + " "+ lname);
	    		return "RegisterSuccess";
	    	}

	    	
	    	@RequestMapping("/logout")
	    	public String logout()
	    	{
	    	  return "index";
	    	}

	    	String testAss="";
	    	
	    	LocalDateTime myDateObj;
	    	DateTimeFormatter myFormatObj;
	    	String formattedDate;
	    	
	    	@GetMapping("/springAssessment.html")
	    	public String spring()
	    	{
	    		testAss = "spring";

	    		myDateObj = LocalDateTime.now();
	    	    myFormatObj = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");

	    	    formattedDate = myDateObj.format(myFormatObj);
	    		return "SpringAssessment";
	    	}
	    	
	    	@GetMapping("/hibernateAssessment.html")
	    	public String hibernate()
	    	{
	    		testAss = "hibernate";
	    		myDateObj = LocalDateTime.now();
	    	    myFormatObj = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");

	    	    formattedDate = myDateObj.format(myFormatObj);
	    		return "HibernateAssessment";
	    	}
	    	
	    	int score1=0;
	    	
	    	@RequestMapping(value="/spring/home",method=RequestMethod.POST)
	    	public @ResponseBody String spring_(@RequestBody int score)
	    	{
	    		score1 = score;
	    		return "hello";
	    	}
	    	
	    	@RequestMapping(value="/hibernate/home",method=RequestMethod.POST)
	    	public @ResponseBody String hibernate_(@RequestBody int score)
	    	{
	    			score1 = score;
	    			return "hello";
	    	}
	    	 
	    	 @GetMapping("/evaluate/spring/test")
	    	 public String Springresult()
	    	 {
	    		 if(score1 >=30)
	    		 {
	    			 Results test = new Results(testAss,formattedDate,score1,50,Email,"passed");
	    			 resrepo.save(test);
	    			 return "PassExam";
	    		 }
	    		 else
	    		 {
	    			 Results test = new Results(testAss,formattedDate,score1,50,Email,"failed");
	    			resrepo.save(test);
	    			 return "FailExam";
	    		 }
	    	 }
	    	 
	    	 @GetMapping("/evaluate/hibernate/test")
	    	 public String hibernateResult()
	    	 {
	    		 if(score1 >=30)
	    		 {
	    			 Results res = new Results(testAss,formattedDate,score1,50,Email,"passed");
	    			 resrepo.save(res);
	    			 return "PassExam";
	    		 }
	    		 else
	    		 {
	    			Results test = new Results(testAss,formattedDate,score1,50,Email,"failed");
	    			 resrepo.save(test);
	    			 return "FailExam";
	    		 }
	    	 }
	    	 
	    		@GetMapping("/admin/all-tests")
	    		public String Testslist(Model mm)
	    		{
	    			List<Results> list = (List<Results>) resrepo.findByTestDate();
	    			mm.addAttribute("test",list);
	    			
	    			return "TestList";
	    		}
	    		
	    		@GetMapping("/admin/all-candidates")
	    		public String Candlist(Model mm)
	    		{
	    			List<Candidate> list = (List<Candidate>) canrepo.findByName();
	    			mm.addAttribute("user",list);
	    			
	    			return "CandidateList";
	    		}
	    	 
	    	 
	    	
	    	
	    	
	    	
	    	
	    	
	    	
	    	
	    	
		}
