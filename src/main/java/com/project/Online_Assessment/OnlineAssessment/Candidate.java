package com.project.Online_Assessment.OnlineAssessment;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;



@Entity
public class Candidate {
	

	
	@Id
	// @GeneratedValue(generator="system-uuid")
//	@GenericGenerator(name="system-uuid", strategy = "uuid")
	
	private String email;
	private String firstname;
	private String lastname;
	
	private String password;
	@Column(name = "UserType")
	private String usertype;
	public Candidate() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	public Candidate(String firstname, String lastname, String email, String password, String userType) {
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.password = password;
		usertype = userType;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUserType() {
		return usertype;
	}
	public void setUserType(String userType) {
		usertype = userType;
	}
	
	

}
